// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBILboqeNqvQ2YBo0dKBmqNsSyDzbkp82I",
  authDomain: "alarma-etrr-esp-32.firebaseapp.com",
  projectId: "alarma-etrr-esp-32",
  storageBucket: "alarma-etrr-esp-32.appspot.com",
  messagingSenderId: "1077068115276",
  appId: "1:1077068115276:web:4da977ea779ffb2295b3e9",
  databaseURL:"https://alarma-etrr-esp-32-default-rtdb.firebaseio.com/"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const auth = getAuth(app);

export {db, auth};